% -*- mode: prolog -*-

:- use_module(library(random)).
:- use_module(library(clpfd)).


% This will be called when the application starts:
user:runtime_entry(start) :-
   %% You may consider putting some other code here...
   write('hello world'),nl,
   write('Getting a random value:'),nl,
   random(1, 10, R),                % from random
   write(R),nl,
   ( all_different([3,9]) ->        % from clpfd
       write('3 != 9'),nl
   ; otherwise ->
       write('3 = 9!?'),nl
   ),
   '$pint'(4711).                   % from our own foreign resource 'bar'

foreign(print_int, '$pint'(+integer)).
foreign_resource(bar, [print_int]).
:- load_foreign_resource(build(bar)).
