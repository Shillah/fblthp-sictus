% This is your main file

:- use_module(library('foobar/foobar')).

main :- X = 1, foobar_fun(X, Y), !, write(X), nl, write(Y), nl.
