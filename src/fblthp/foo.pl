% -*- mode: prolog -*-

:- use_module(library(system)).
:- use_module(library(clpfd)).
:- use_module(library('foobar/foobar')).

:- load_foreign_resource(build(bar)).

% This will be called when the application starts:
user:runtime_entry(start) :-
    %% You may consider putting some other code here...
    write('hello world'),nl,
    write('Getting date:'),nl,
    datime(Date),             % from system
    write(Date),nl,
    ( all_different([3,9]) ->        % from clpfd
      write('3 != 9'),nl
    ; otherwise ->
      write('3 = 9!?'),nl
    ),
    '$pint'(4711),                   % from our own foreign resource 'bar'
    X = 1, foobar_fun(X, Y), !, write(X), nl, write(Y), nl.
