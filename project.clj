(defproject fblthp "0.1.0"
  :license "NONE"
  :url "http://example.com/fblthp-sicstus"
  :run [[main [src "main"] main false]]
  :splfr {bar {:input [[src "bar.pl"]
                       [src "bar.c"]],
               :output [build "bar.so"]}
          bar-static {:input [[src "bar.pl"]
                              [src "bar.c"]]
                      :output [build "bar.so"]
                      :flags [:static]}}
  :spld  {bar {:flags [:static :verbose],
               :main  :restore,
               :respath ["build"],
               :create-sav {:output [build "bar.sav"]
                            :inputs [[src   "bar.pl"]]}
               :resources ["bar.sav=build/bar.sav"]
               :output [build "bar"]}}
  :vendors [sicstus]
  :dependencies [[gitlab.Shillah/foobar "3619ef7c"]])
